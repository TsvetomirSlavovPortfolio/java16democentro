package sealedtypes.garden;

// the extended classes must be declared as final, sealed or non-sealed. We can also seal an abstract class.
public sealed class Plant permits Herb, Shrub, Climber {
}

final class Herb extends Plant {}
non-sealed class Shrub extends Plant {}
sealed class Climber extends Plant permits Cucumber {}

final class Cucumber extends Climber {}
