package sealedtypes.garden;

public class Gardner {

    int processType(Plant plant) {
        if (plant instanceof Cucumber) {
            return cucumberHarvest();
        } else if (plant instanceof Climber) {
            return climberSow();
        } else if (plant instanceof Herb) {
            return herbSell();
        } else if (plant instanceof Shrub) {
            return shrubPrune();
        } else {
            // so here is the issue, I don't have control over the subclasses of Plant
            // we have to write a general solution for a specific problem
            System.out.println("Control should not reach here!");
            return 0;
            // after we have sealed the Plant class, we don't need this path any more, but the current structure
            // of the if else requires it.
        }
    }

    private int cucumberHarvest() {
        return 1;
    }

    private int climberSow() {
        return 2;
    }

    private int herbSell() {
        return 3;
    }

    private int shrubPrune() {
        return 4;
    }

    // This code doesn't work in Java 16. It would work in a future Java version after the addition of
    // type-tested-pattern to the switch expression
    int processInAFutureJavaVersion(Plant plant) {

        /*
        switch (plant) {
            case Cucumber c -> c.cucumberHarvest();
            case Climber cl -> cl.climberSow();
            case Herb h -> h.herbSell();
            case Shrub s -> s.shrubPrune();
        }
        */
        return 0;
    }

}
