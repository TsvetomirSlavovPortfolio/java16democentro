package sealedtypes.interfaces;

// only sealed or non-sealed for interfaces
public sealed interface Move {
}

final class Athlete implements Move {}
non-sealed interface Jump extends Move {}
sealed interface Kick extends Move {}

final class Karate implements Kick {}
