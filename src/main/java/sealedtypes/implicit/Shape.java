package sealedtypes.implicit;

// Implicit subclasses. If you define the derived classes in the same source file, you can omit the permits keyword
// in the declaration of the sealed class

public sealed class Shape {
}

non-sealed class Circle extends Shape {}
non-sealed class Quadrilateral extends Shape {}
