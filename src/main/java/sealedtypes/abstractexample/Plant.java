package sealedtypes.abstractexample;

// the extended classes must be final, sealed or non-sealed. We can also seal an abstract class.
public sealed abstract class Plant {
    abstract void grow();
}

final class Herb extends Plant {
    @Override
    void grow() {

    }
}

non-sealed abstract class Shrub extends Plant {
    @Override
    void grow() {

    }
}

sealed class Climber extends Plant permits Cucumber {
    @Override
    void grow() {

    }
}

final class Cucumber extends Climber {}
