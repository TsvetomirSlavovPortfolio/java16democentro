package sealedtypes.codeanalysis;

// Stronger code analysis with closed list of subclasses
// With sealed classes and interfaces, you can have an explicit list of inheritors that
// is known to the compiler, IDE and the runtime (via reflection).
// This closed list of subclasses makes the code analysis more powerful.

// For example, consider the following completely sealed hierarchy of WritingDevice
// (which doesn’t have non-sealed subtypes):
// Now, instanceof and casts can check the complete hierarchy statically.

public interface Erasable {
}

sealed class WritingDevice permits Pen, Pencil {}
final class Pencil extends WritingDevice {}
sealed class Pen extends WritingDevice permits Marker {}
final class Marker extends Pen {}