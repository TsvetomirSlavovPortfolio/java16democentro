package sealedtypes.codeanalysis;

//Code on line1 and line2 are compilation errors. The compiler checks all the inheritors
// from the permits list and finds that no one of them implements the
// Erasable or the CharSequence interface:
public class UseWritingDevice {
//    static void write(WritingDevice pen) {
//        if (pen instanceof Erasable) {                   // line1
//        }
//        CharSequence charSequence = ((CharSequence) pen);// line2
//    }
}
