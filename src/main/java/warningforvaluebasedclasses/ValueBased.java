package warningforvaluebasedclasses;

public class ValueBased {
    void shouldWarnForWrapperConstructors() {
        // these were deprecated in Java 9, but for now for #JDK16 marked for removal (JEP 390)
        // Designate the primitive wrapper classes as value-based
        // and deprecate their constructors for removal, prompting new deprecation warnings.

        //Double aDouble = new Double(1.0);
        Double aDouble = 1.0;
    }
}
