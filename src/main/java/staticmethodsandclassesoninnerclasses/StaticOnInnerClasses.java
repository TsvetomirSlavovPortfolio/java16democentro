package staticmethodsandclassesoninnerclasses;

public class StaticOnInnerClasses {

    static class Outer {

        class NonStaticInnerClass {
            void instanceMethod() {
                System.out.println(Outer.this);
            }

            // #JDK16: static methods on inner classes
            static void staticMethod() {
                // Outer.this is not accessible here
            }

            // #JDK16: static classes inside inner classes
            static class StaticInnerClass {

            }
        }
    }

    public static void main(String[] args) {
        new Runnable() {
            @Override
            public void run() {

            }

            // #JDK16: static methods inside anonymous inner classes
            static void staticMethods() {

            }
        };
    }

}
