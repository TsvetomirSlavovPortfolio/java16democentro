package streams;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsNewMethod {
    record Stock(String name, double value) { }

    List<String> unmodifiableListCollectorJava10(List<Stock> allStocks, LocalDateTime dateTime) {
        record TopStock(Stock stock, double value) { }

        return allStocks.stream()
                .map(s -> new TopStock(s, getStockValue(s, dateTime)))
                .sorted(Comparator.comparingDouble(TopStock::value).reversed())
                .limit(2)
                .map(s -> s.stock().name())
                .collect(Collectors.toUnmodifiableList());
    }

    List<String> unmodifiableListCollectorJava16(List<Stock> allStocks, LocalDateTime dateTime) {
        // TopStock is a local record
        record TopStock(Stock stock, double value) { }

        return allStocks.stream()
                .map(s -> new TopStock(s, getStockValue(s, dateTime)))
                .sorted(Comparator.comparingDouble(TopStock::value).reversed())
                .limit(2)
                .map(s -> s.stock().name())
                .toList();
    }

    public double getStockValue(Stock stock, LocalDateTime dateTime) {
        return stock.value();
    }
}
