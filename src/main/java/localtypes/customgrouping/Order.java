package localtypes.customgrouping;

import java.time.LocalDate;

public record Order(int orderId, LocalDate localDate, double amount) {

}
