package localtypes.customgrouping;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Filter {

    private Set<Person> filterForPeopleWithFiveOrders(List<Person> people){
        // local records provide a way to group together different records in a specific way
        // now that we have this record of a person and orders we can use it in Streams API ang get what we need
        record PersonAndOrders(Person person, List<Order> orders) { }

        return people.stream()
                     .map(person -> new PersonAndOrders(person, getOrdersFor(person)))
                     .filter(personAndOrders -> personAndOrders.orders().size() == 5)
                     .map(PersonAndOrders::person)
                     .collect(Collectors.toUnmodifiableSet());
    }

    List<Order> getOrdersFor(Person person) {
        String personName = person.firstName();
        List<Order> orders = new ArrayList<>();
        orders.add(new Order(53415, LocalDate.now(), 67.17));
        return orders;
    }


}
