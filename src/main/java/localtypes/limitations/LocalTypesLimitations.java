package localtypes.limitations;

// Local records/enums/interfaces. This change was introduced as part of the Records JEP (in #JDK15)
// even though it's not just for records.
// Preview feature in #JDK15
// Standard feature in #JDK16
public class LocalTypesLimitations {
    record Stock(String name, double value) { }

    public void createLocalInterface() {
        interface LocalInterface {
            void purchase();
        }
        // Code to use LocalInterface
    }

    public void createLocalEnum() {
        enum Color {RED, YELLOW, BLUE}
        // Code to use enum Color
    }

    public void createLocalRecord() {
        record Agency(String name, double maxBid) { }
    }

    //However, they cannot capture any context variable. For example, for the local enum Data,
    // the enum constants FOO and BAR can’t be created using the method parameter input in the method test():
    void test(int input) {
        enum Data {
//            FOO(input), BAR(input*2); // Error. Can’t refer to input
//            private final int i;
//            Data(int i) {
//                this.i = i;
//            }
        }
    }
}
