package patternmatching;

// Simplifying conditional expressions
public class Monitor {

    String model;
    double price;

    // here is an example how we usually override the equals() method fo a class
    @Override
    public boolean equals(Object o) {
        if (o instanceof Monitor) {
            Monitor other = (Monitor) o;
            if (model.equals(other.model) && price == other.price) {
                return true;
            }
        }
        return false;
    }

    // here is the same but simplified by using pattern matching for instance of.
    // IntelliJ highlights other for context actions
    // Then click and hover over the first if statement and merge the nested if statements
    // and then one more time over the if and select simplify
    public boolean equalsA(Object o) {
        return o instanceof Monitor other
                && model.equals(other.model)
                && price == other.price;
    }

}
