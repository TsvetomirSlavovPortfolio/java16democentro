package patternmatching;

// Preview feature in Java 14 and 15 and now a standard feature in Java 16
public class PatternMatching {

    void outputValueInUppercase(Object obj) {
        if (obj instanceof String) {
            String strValue = ((String) obj);
            // now we are bale to access methods specific to class String
            System.out.println(strValue.toUpperCase());
        }
    }

    // there is an obvious repetition here
    // let's introduce a pattern variable by invoking context actions and replacing with pattern variable
    void outputValueInUppercaseVersionTwo(Object obj) {
        // if the condition is true, this pattern variable binds to the instance being compared
        // so we don't need additional variable declaration or explicit casting
        if (obj instanceof String strValue) {
            System.out.println(strValue.toUpperCase());
        } else {
            // the scope of the pattern variable is limited to the if statement
            //System.out.println(strValue.toUpperCase());
        }
    }

    // we can introduce a variable with the same name and see how the this affects the code
    // if the if statement is negated the variables will switch their reference,
    // now the in the if we are referring to the field
    private String strValue = "A field";

    void outputValueInUpperCaseVersionThree(Object obj) {
        if (!(obj instanceof String strValue)) {
            System.out.println(strValue.toUpperCase());
        } else {
            System.out.println(strValue.toUpperCase());
        }
    }

    // in Java 16 pattern variables are not final, you change their values.
    // if you want though, you can add the final modifier to their declaration in the if condition - final String strValue
    void outputValueInUppercaseVersionThree(Object obj) {
        if (obj instanceof String strValue) {
            strValue = strValue.replace("m", "k");
            System.out.println(strValue.toUpperCase());
        }
    }


    // using the supertype to as a pattern matching won't compile
    void outputValueInUppercaseVersionFour(String str) {
//        if (str instanceof Object strValue) {
//
//        }
    }
}
