package records.generics;

public record Parcel<T>(T contents, double length, double breadth, double height, double weight) {
}
