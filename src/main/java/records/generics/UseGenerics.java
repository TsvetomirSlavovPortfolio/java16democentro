package records.generics;

public class UseGenerics {
    public static void main(String[] args) {
        Parcel<Table> parcel = new Parcel<>(new Table(), 200, 100, 55, 136.88);
        System.out.println(parcel);
    }
}

class Table {

}
