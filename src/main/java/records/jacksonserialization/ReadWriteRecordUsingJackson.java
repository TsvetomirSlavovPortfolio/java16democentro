package records.jacksonserialization;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

public class ReadWriteRecordUsingJackson {

    public static void main(String[] args) {
        // 1. Persist record Rectangle using Jackson version 2.11.3 with annotations or version 2.12.2 without annotations
        Rectangle rectangle = new Rectangle(20, 60);
        writeToFileUsingJackson(rectangle);
        System.out.println(readFromFileUsingJackson());
    }

    static void writeToFileUsingJackson(Object obj) {
        try {
            new ObjectMapper().writeValue(new FileOutputStream(getFile()), obj);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static Object readFromFileUsingJackson() {
        Object returnValue = null;
        try {
            returnValue = new ObjectMapper().readValue(new FileInputStream(getFile()), Rectangle.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    private static File getFile() {
        return new File("mydata.json");
    }

}
