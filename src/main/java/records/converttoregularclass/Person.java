package records.converttoregularclass;

import java.io.Serializable;

// if the Java version does not support records we can quickly convert our records to regular classes by using the context action
public record Person(String name, int age) implements Serializable {
}
