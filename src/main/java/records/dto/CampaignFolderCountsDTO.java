package records.dto;

import java.io.Serializable;

// Immutable class with only final fields and only getters for these fields can be converted to a record
public class CampaignFolderCountsDTO implements Serializable {

    private final int subFolderCount;
    private final int campaignGroupCount;
    private final int campaignCount;

    public CampaignFolderCountsDTO(int subFolderCount, int campaignGroupCount, int campaignCount) {
        this.subFolderCount = subFolderCount;
        this.campaignGroupCount = campaignGroupCount;
        this.campaignCount = campaignCount;
    }

    public int getSubFolderCount() {
        return subFolderCount;
    }

    public int getCampaignGroupCount() {
        return campaignGroupCount;
    }

    public int getCampaignCount() {
        return campaignCount;
    }

}
