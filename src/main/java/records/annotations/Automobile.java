package records.annotations;

import org.jetbrains.annotations.NotNull;

public record Automobile(Engine engine, Purpose purpose, Accelerate accelerate, int wheels, @NotNull String name) {
}

record Engine(String series) { }

enum Purpose { COMMERCIAL, PERSONAL }

interface Accelerate { }
