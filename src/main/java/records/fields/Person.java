package records.fields;

import java.io.Serializable;

public record Person(String name, int age) implements Serializable {

    //We can't have instance variables in a record
    //private final String lName;
    static private int instanceCtr;

    public Person {
        instanceCtr++;
    }

    static int getInstanceCtr() {
        return instanceCtr;
    }

    String getNameInUpperCase() {
        return name().toUpperCase();
    }

}
