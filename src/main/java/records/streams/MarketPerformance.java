package records.streams;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MarketPerformance {
    record Stock(String name, double value) { }

    List<String> getTopPerformingStocks(List<Stock> allStocks, LocalDateTime dateTime) {
        record TopStock(Stock stock, double value) { }

        return allStocks.stream()
                        .map(s -> new TopStock(s, getStockValue(s, dateTime)))
                        .sorted(Comparator.comparingDouble(TopStock::value).reversed())
                        .limit(2)
                        .map(s -> s.stock().name())
                        .collect(Collectors.toList());
    }

    public double getStockValue(Stock stock, LocalDateTime dateTime) {
        return stock.value();
    }
}
