package records.recordasacomponent;

// record Automobile declares it's components as primitive, class, record, interface and enum
public record Automobile(Engine engine, Purpose purpose, Accelerate accelerate, int wheels, String name) {
}

record Engine(String series) { }

enum Purpose { COMMERCIAL, PERSONAL }

interface Accelerate { }
