package records.serialization;

import java.io.Serializable;
import java.util.Objects;

public class PersonClass implements Serializable {
    final private String name;
    final private int age;

    public PersonClass(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonClass personDeleted = (PersonClass) o;
        return age == personDeleted.age && Objects.equals(name, personDeleted.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
