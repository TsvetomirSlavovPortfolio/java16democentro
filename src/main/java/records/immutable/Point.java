package records.immutable;

public record Point(int x, int y) {
}
