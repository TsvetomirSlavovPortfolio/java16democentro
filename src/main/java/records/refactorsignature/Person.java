package records.refactorsignature;

import java.io.Serializable;

public record Person(String name, int age) implements Serializable {
    // Modify the order of the components of the constructor
    public Person(String name, int age) {
        if (age < 0) {
            throw new IllegalArgumentException("Age < 0");
        }
        this.name = name;
        this.age = age;
    }

    public static void main(String[] args){
        final var student = new Person("Tsvetomir", 22);
    }
}
