package records.extendingisnotallowed;

import java.time.LocalDate;

public record Order(int orderId, LocalDate localDate, double amount) {

}
