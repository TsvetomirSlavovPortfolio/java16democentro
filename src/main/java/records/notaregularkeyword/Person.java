package records.notaregularkeyword;

import java.io.Serializable;

public record Person(String name, int age) implements Serializable {
    // record is a restricted identifier (like var), but it isn’t a regular keyword (yet).
    // So, the following code is valid but not recommended as more developers start using Java 16

    // a variable named record
    static int record;

    // a method named record
    static int record() {
        return 2;
    };
}
